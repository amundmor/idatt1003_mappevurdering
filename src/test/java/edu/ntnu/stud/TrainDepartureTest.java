package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.stud.application.TrainDeparture;

import java.time.DateTimeException;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class of test methods for the TrainDeparture class.
 *
 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @see TrainDeparture
 * @since v0.2
 */
public class TrainDepartureTest {

  public TrainDeparture correctTrainDepartureExample;

  /**
   * This method is run before each test.
   * It sets up a fresh TrainDeparture object with known parameters used for testing where applicable.
   */
  @BeforeEach
  public void setupPositiveTest() {
    correctTrainDepartureExample = new TrainDeparture(
            LocalTime.of(10, 30),
            "L1",
            1,
            "Oslo",
            2,
            LocalTime.of(0, 0));
  }

  @Nested
  @DisplayName("Collection of positive outcome tests")
  public class TrainDeparturePositiveTests {


    @Test
    @DisplayName("Constructor does not throw exception on valid parameters")
    public void constructorDoesNotThrowException() {
      try {
        assertNotNull(correctTrainDepartureExample);
      } catch (IllegalArgumentException iae) {
        fail("Exception thrown: " + iae.getMessage());
      }
    }

    @Test
    @DisplayName("Getters return correct values")
    public void gettersReturnCorrectValues() {
      try {
        assertEquals(LocalTime.of(10, 30), correctTrainDepartureExample.getDepartureTime());
        assertEquals("L1", correctTrainDepartureExample.getLine());
        assertEquals(1, correctTrainDepartureExample.getTrainNumber());
        assertEquals("Oslo", correctTrainDepartureExample.getDestination());
        assertEquals(2, correctTrainDepartureExample.getTrack());
        assertEquals(LocalTime.of(0, 0), correctTrainDepartureExample.getDelay());
      } catch (Exception e) {
        fail("Exception thrown" + e.getMessage());
      }
    }

    @Test
    @DisplayName("Setters set correct values and returns true on success")
    public void settersSetCorrectValuesAndReturnsTrueOnSuccess() {
      boolean setTrackBol = correctTrainDepartureExample.setTrack(3);
      boolean setDelayBol = correctTrainDepartureExample.setDelay(1, 0);

      assertTrue(setTrackBol);
      assertTrue(setDelayBol);
      assertEquals(3, correctTrainDepartureExample.getTrack());
      assertEquals(LocalTime.of(1, 0), correctTrainDepartureExample.getDelay());
    }

    @Test
    //@DisplayName("toTable returns correct format String");
    public void toTableReturnsCorrectFormatStringList() {
      String[] correctResult = {LocalTime.of(10, 30).toString(), "L1", "1", "Oslo", "", "2"};
      String[] result = correctTrainDepartureExample.toTable();
      assertArrayEquals(correctResult, result);
    }
  }

  @Nested
  @DisplayName("Collection of negative outcome tests")
  public class TrainDepartureNegativeTests {

    @Test
    @DisplayName("LocalTime throws exception on departureTime OutOfBounds")
    public void LocalTimeThrowsExceptionOnDepartureTimeOutOfBounds() {
      assertThrows(DateTimeException.class, () -> new TrainDeparture(
              LocalTime.of(24, 60),
              "L1",
              1,
              "Oslo",
              2,
              LocalTime.of(0, 0)));
    }

    @Test
    @DisplayName("LocalTime throws exception on departureTime with negative hours")
    public void LocalTimeThrowsExceptionOnDepartureTimeWithNegativeHours() {
      assertThrows(DateTimeException.class, () -> new TrainDeparture(
              LocalTime.of(-10, 30), "L1",
              1,
              "Oslo",
              2,
              LocalTime.of(0, 0)));
    }

    @Test
    @DisplayName("Constructor throws exception on empty line")
    public void constructorThrowsExceptionOnEmptyLine() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
              LocalTime.of(10, 30),
              "",
              1,
              "Oslo",
              2,
              LocalTime.of(0, 0)));
    }

    @Test
    @DisplayName("Constructor throws exception on negative train number")
    public void constructorThrowsExceptionOnNegativeTrainNumber() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
              LocalTime.of(10, 30),
              "L1",
              -1,
              "Oslo",
              2,
              LocalTime.of(0, 0)));
    }

    @Test
    @DisplayName("Constructor throws exception on empty destination")
    public void constructorThrowsExceptionOnEmptyDestination() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
              LocalTime.of(10, 30),
              "L1",
              1,
              "",
              2,
              LocalTime.of(0, 0)));
    }

    @Test
    @DisplayName("LocalTime throws exception on delay out of bounds")
    public void LocalTimeThrowsExceptionOnDelayOutOfBounds() {
      assertThrows(DateTimeException.class, () -> new TrainDeparture(
              LocalTime.of(10, 30),
              "L1",
              1,
              "Oslo",
              2,
              LocalTime.of(24, 60)));
    }

    @Test
    @DisplayName("LocalTime throws exception on delay with negative hours")
    public void LocalTimeThrowsExceptionOnDelayWithNegativeHours() {
      assertThrows(DateTimeException.class, () -> new TrainDeparture(
              LocalTime.of(10, 30),
              "L1",
              1,
              "Oslo",
              2,
              LocalTime.of(-1, 0)));
    }

    @Test
    @DisplayName("Setters return false when not successful")
    public void settersReturnFalseWhenNotSuccessful() {
      try {
        boolean setDelayResult = correctTrainDepartureExample.setDelay(-1, 0);
        boolean setTrackResult = correctTrainDepartureExample.setTrack(-2);

        assertFalse(setDelayResult);
        assertFalse(setTrackResult);
        assertEquals(LocalTime.of(0, 0), correctTrainDepartureExample.getDelay());
        assertEquals(2, correctTrainDepartureExample.getTrack());
      } catch (Exception e) {
        fail("Exception thrown" + e.getMessage());
      }
    }
  }
}