package edu.ntnu.stud;

import org.junit.jupiter.api.Test;

import edu.ntnu.stud.application.DepartureRegister;
import edu.ntnu.stud.application.TrainDeparture;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;

/**
 * Class of test methods for the DepartureRegister class.
 *
 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @see DepartureRegister
 * @since v0.2
 */
public class DepartureRegisterTest {
  private static DepartureRegister departureRegisterWithDepartures;

  /**
   * This method is run before each test.
   * It sets up a fresh DepartureRegister object with 5 known TrainDepartures used for testing where applicable.
   */
  @BeforeEach
  public void init() {
    departureRegisterWithDepartures = new DepartureRegister();
    departureRegisterWithDepartures.addTrainDeparture(
            LocalTime.of(10, 30),
            "L1",
            1,
            "Oslo",
            2,
            LocalTime.of(0, 0));
    departureRegisterWithDepartures.addTrainDeparture(
            LocalTime.of(11, 30),
            "L1",
            2,
            "Oslo",
            2,
            LocalTime.of(0, 0));
    departureRegisterWithDepartures.addTrainDeparture(
            LocalTime.of(12, 30),
            "L1",
            3,
            "Drammen",
            2,
            LocalTime.of(0, 0));
    departureRegisterWithDepartures.addTrainDeparture(
            LocalTime.of(13, 30),
            "L1",
            4,
            "Kongsberg",
            2,
            LocalTime.of(0, 0));
    departureRegisterWithDepartures.addTrainDeparture(
            LocalTime.of(14, 30),
            "L1",
            5,
            "Kongsberg",
            2,
            LocalTime.of(0, 0));
  }

  @Nested
  @DisplayName("Collection of positive outcome tests")
  public class PositiveDepartureRegisterTest {

    @Test
    @DisplayName("Default constructor does not throw exception on valid parameters")
    public void defaultConstructorDoesNotThrowException() {
      try {
        DepartureRegister departureRegister = new DepartureRegister();
        assertNotNull(departureRegister);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Alternative constructor does not throw exception on valid parameters")
    public void alternativeConstructorDoesNotThrowException() {
      try {
        ArrayList<TrainDeparture> testTrainDeparture = new ArrayList<>();
        DepartureRegister departureRegister = new DepartureRegister(testTrainDeparture);
        assertNotNull(departureRegister);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getRegisterArray() returns correct ArrayList")
    public void getRegisterArrayReturnsSuccessfully() {
      try {
        ArrayList<TrainDeparture> resultRegisterArray = departureRegisterWithDepartures.getRegisterArray();

        assertEquals(resultRegisterArray.size(), departureRegisterWithDepartures.getRegisterArray().size());
        assertEquals(resultRegisterArray.get(0), departureRegisterWithDepartures.getRegisterArray().get(0));
        assertEquals(resultRegisterArray.get(1), departureRegisterWithDepartures.getRegisterArray().get(1));
        assertEquals(resultRegisterArray.get(2), departureRegisterWithDepartures.getRegisterArray().get(2));
        assertEquals(resultRegisterArray.get(3), departureRegisterWithDepartures.getRegisterArray().get(3));
        assertEquals(resultRegisterArray.get(4), departureRegisterWithDepartures.getRegisterArray().get(4));
        assertEquals(5, resultRegisterArray.size());

      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("addTrainDeparture() returns true on successful addition")
    public void addTrainDepartureReturnsTrueOnSuccessfulAddition() {
      try {
        boolean addition = departureRegisterWithDepartures.addTrainDeparture(LocalTime.of(15, 30),
                "L1",
                6,
                "Kongsberg",
                2,
                LocalTime.of(0, 0));

        assertTrue(addition);
        assertEquals(6, departureRegisterWithDepartures.getRegisterArray().size());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("removeTrainDeparture() returns true on successful removal")
    public void removeTrainDepartureReturnsTrueOnSuccessfullRemoval() {
      try {
        boolean removal = departureRegisterWithDepartures.removeTrainDeparture(1);

        assertTrue(removal);
        assertEquals(4, departureRegisterWithDepartures.getRegisterArray().size());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDeparture() returns correct TrainDeparture")
    public void getDepartureReturnsCorrectTrainDeparture() {
      try {
        TrainDeparture departure = departureRegisterWithDepartures.getDepartureByTrainNumber(1);

        assertEquals(departureRegisterWithDepartures.getRegisterArray().get(0), departure);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDeparturesGoingToDestination() returns correct TrainDepartures in ArrayList")
    public void getDeparturesGoingToDestinationReturnsCorrectTrainDeparturesInArrayList() {
      try {
        ArrayList<TrainDeparture> resultArray =
                departureRegisterWithDepartures.getDeparturesGoingToDestination("Kongsberg");

        assertEquals(departureRegisterWithDepartures.getRegisterArray().get(3), resultArray.get(0));
        assertEquals(departureRegisterWithDepartures.getRegisterArray().get(4), resultArray.get(1));
        assertEquals(2, resultArray.size());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("editDeparture() correctly edits TrainDeparture object")
    public void editDepartureCorrectlyEditsObject() {
      try {
        boolean resultBoolean = departureRegisterWithDepartures.editDeparture(
                1,
                3,
                LocalTime.of(0, 1));

        assertTrue(resultBoolean);
        assertEquals(3, departureRegisterWithDepartures.getDepartureByTrainNumber(1).getTrack());
        assertEquals(LocalTime.of(0, 1),
                departureRegisterWithDepartures.getDepartureByTrainNumber(1).getDelay());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDepartureTableData() returns correct ArrayList")
    public void getDepartureTableDataReturnsCorrectArrayList() {
      try {
        // removes some departures for simplicity
        departureRegisterWithDepartures.removeTrainDeparture(3);
        departureRegisterWithDepartures.removeTrainDeparture(4);
        departureRegisterWithDepartures.removeTrainDeparture(5);

        ArrayList<String[]> correctStringListArray = new ArrayList<>();
        String[] departureOne = {"10:30", "L1", "1", "Oslo", "", "2"};
        String[] departureTwo = {"11:30", "L1", "2", "Oslo", "", "2"};
        correctStringListArray.add(departureOne);
        correctStringListArray.add(departureTwo);

        ArrayList<String[]> resultStringListArray = departureRegisterWithDepartures.getDepartureTableData();

        assertArrayEquals(correctStringListArray.get(0), resultStringListArray.get(0));
        assertArrayEquals(correctStringListArray.get(1), resultStringListArray.get(1));
        assertEquals(2, resultStringListArray.size());

      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDepartureTableDataGoingToDestination() returns correct ArrayList matching filter parameters")
    public void getDepartureTableDataGoingToDestinationReturnsCorrectArrayListMatchingFilterParameters() {
      try {
        departureRegisterWithDepartures.removeTrainDeparture(3);
        departureRegisterWithDepartures.removeTrainDeparture(4);
        departureRegisterWithDepartures.removeTrainDeparture(5);
        ArrayList<TrainDeparture> correctArray = departureRegisterWithDepartures.getRegisterArray();

        ArrayList<TrainDeparture> resultArray = departureRegisterWithDepartures.getDeparturesGoingToDestination("Oslo");

        assertEquals(correctArray.get(0), resultArray.get(0));
        assertEquals(correctArray.get(1), resultArray.get(1));
        assertEquals(2, resultArray.size());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("checkDepartureExistence() returns true on valid parameter")
    public void checkDepartureExistenceReturnsTrueOnValidParameter() {
      try {
        boolean resultBoolean = departureRegisterWithDepartures.checkDepartureExistence(1);

        assertTrue(resultBoolean);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDepartureTableDataWithinTimeFrame() returns departures within time frame parameter")
    public void getDepartureTableDataWithinTimeFrameReturnsDeparturesWithinTimeFrameParameter() {
      try {
        ArrayList<String[]> resultStringListArray =
                departureRegisterWithDepartures.getDepartureTableDataWithinTimeFrame(
                        LocalTime.of(10, 29),
                        LocalTime.of(11, 31));

        departureRegisterWithDepartures.removeTrainDeparture(3);
        departureRegisterWithDepartures.removeTrainDeparture(4);
        departureRegisterWithDepartures.removeTrainDeparture(5);
        ArrayList<String[]> correctStringListArray = departureRegisterWithDepartures.getDepartureTableData();

        assertArrayEquals(correctStringListArray.get(0), resultStringListArray.get(0));
        assertArrayEquals(correctStringListArray.get(1), resultStringListArray.get(1));
        assertEquals(2, resultStringListArray.size());

      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

  }

  @Nested
  @DisplayName("Collection of negative outcome tests")
  public class NegativeDepartureRegisterTest {

    @Test
    @DisplayName("addTrainDeparture() returns false on unsuccessful addition")
    public void addTrainDepartureReturnsFalseOnUnsuccessfulAddition() {
      try {
        boolean resultBooleanExistingTrainNumber = departureRegisterWithDepartures.addTrainDeparture(
                LocalTime.of(15, 30),
                "L1", 4,
                "Kongsberg",
                2,
                LocalTime.of(0, 0));
        boolean resultBooleanEmptyLine = departureRegisterWithDepartures.addTrainDeparture(
                LocalTime.of(15, 30),
                "",
                8,
                "Kongsberg",
                2,
                LocalTime.of(0, 0));
        boolean resultBooleanTrackOutOfBounds = departureRegisterWithDepartures.addTrainDeparture(
                LocalTime.of(15, 30),
                "L1",
                9,
                "Kongsberg",
                -2,
                LocalTime.of(0, 0));

        assertFalse(resultBooleanExistingTrainNumber);
        assertFalse(resultBooleanEmptyLine);
        assertFalse(resultBooleanTrackOutOfBounds);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("removeTrainDeparture() returns false on unsuccessful removal")
    public void removeTrainDepartureReturnsFalseOnUnsuccessfulRemoval() {
      try {
        boolean removal = departureRegisterWithDepartures.removeTrainDeparture(6);

        assertFalse(removal);
        assertEquals(5, departureRegisterWithDepartures.getRegisterArray().size());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDeparture() throws exception on invalid TrainNumber")
    public void getDepartureThrowsExceptionOnInvalidTrainNumber() {
      assertThrows(IllegalArgumentException.class, () ->
              departureRegisterWithDepartures.getDepartureByTrainNumber(6));
    }

    @Test
    @DisplayName("getDeparturesGoingToDestination() returns empty ArrayList on invalid destination")
    public void getDeparturesGoingToDestinationReturnsEmptyArrayListOnInvalidDestination() {
      try {
        ArrayList<TrainDeparture> departures =
                departureRegisterWithDepartures.getDeparturesGoingToDestination("Bergen");

        assertEquals(0, departures.size());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("editDeparture() returns false on unsuccessful edit")
    public void editDepartureReturnsFalseOnUnsuccessfulEdit() {
      try {
        boolean resultBooleanInvalidTrainNumber = departureRegisterWithDepartures.editDeparture(
                6,
                3,
                LocalTime.of(0, 0));
        boolean resultBooleanInvalidTrack = departureRegisterWithDepartures.editDeparture(
                1,
                -2,
                LocalTime.of(0, 0));

        assertFalse(resultBooleanInvalidTrainNumber);
        assertFalse(resultBooleanInvalidTrack);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("checkDepartureExistence() returns false on not existing trainNumber")
    public void checkDepartureExistenceReturnsFalseOnNotExistingTrainNumber() {
      try {
        boolean resultBoolean = departureRegisterWithDepartures.checkDepartureExistence(6);

        assertFalse(resultBoolean);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDepartureTableDataWithinTimeFrame() returns full table data if startTime isAfter endTime")
    public void getDepartureTableDataWithinTimeFrameReturnsFullTableDataIfStartTimeIsAfterEndTime() {
      try {
        ArrayList<String[]> correctStringListArray = departureRegisterWithDepartures.getDepartureTableData();

        ArrayList<String[]> resultStringListArray =
                departureRegisterWithDepartures.getDepartureTableDataWithinTimeFrame(
                        LocalTime.of(10, 30),
                        LocalTime.of(10, 29));

        assertArrayEquals(correctStringListArray.get(0), resultStringListArray.get(0));
        assertArrayEquals(correctStringListArray.get(1), resultStringListArray.get(1));
        assertArrayEquals(correctStringListArray.get(2), resultStringListArray.get(2));
        assertArrayEquals(correctStringListArray.get(3), resultStringListArray.get(3));
        assertArrayEquals(correctStringListArray.get(4), resultStringListArray.get(4));
        assertEquals(5, resultStringListArray.size());
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }
  }
}