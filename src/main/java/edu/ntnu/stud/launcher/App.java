package edu.ntnu.stud.launcher;

import edu.ntnu.stud.application.UserInterface;

/**
 * This is the main entry point for the train dispatch application.
 * It contains the main loop for the application.

 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @since v0.1
 */
public class App {
  public static void main(String[] args) {
    UserInterface.init();
    UserInterface.start();
  }
}