package edu.ntnu.stud.application;

import edu.ntnu.stud.utils.Validator;
import java.time.LocalTime;

/**
 * <p>This class models a single TrainDeparture.
 * It also contains methods that interacts with its own data.</p>
 *
 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @since v0.1
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private LocalTime delay;

  // ---- Constructor ----

  /**
   * Constructor for TrainDeparture
   * If "null" is given as delay, it sets delay to LocalTime 00:00.
   *
   * @param departureTime trains departure time
   * @param line          trains operating line
   * @param trainNumber   train number
   * @param destination   trains destination
   * @param track         track number train arrives on
   * @param delay         potential delay
   */
  public TrainDeparture(
          LocalTime departureTime,
          String line, int trainNumber,
          String destination, int track,
          LocalTime delay) throws IllegalArgumentException {
    if (Validator.validateNotEmptyString(line) == false) {
      throw new IllegalArgumentException("Line can not be empty");
    }
    if (Validator.validatePositiveInt(trainNumber) == false) {
      throw new IllegalArgumentException("Train number must be a positive number.");
    }
    if (Validator.validateNotEmptyString(destination) == false) {
      throw new IllegalArgumentException("Destination can not be empty");
    }
    if (Validator.validateTrackInt(track) == false) {
      throw new IllegalArgumentException("Track must be a positive number or -1");
    }

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;

    // -- Delay --
    // set to 00:00 if no delay.
    if (delay == null) {
      this.delay = LocalTime.of(00, 00);
    } else {
      this.delay = delay;
    }
  }

  // ---- Getters ----
  public LocalTime getDepartureTime() {
    return this.departureTime;
  }

  public String getLine() {
    return this.line;
  }

  public int getTrainNumber() {
    return this.trainNumber;
  }

  public String getDestination() {
    return this.destination;
  }

  public int getTrack() {
    return this.track;
  }

  public LocalTime getDelay() {
    return this.delay;
  }

  // ---- Setters ----

  /**
   * Sets track to the given int.
   * Returns true if set was successful.
   *
   * @param track track number train arrives on
   * @return boolean
   */
  public boolean setTrack(int track) {
    try {
      if (Validator.validateTrackInt(track) == false) {
        throw new IllegalArgumentException("Track number can not be less than one");
      }
      this.track = track;
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Sets delay to the given LocalTime object
   * Returns true if set was successfull.
   *
   * @param hourInput   delay hours
   * @param minuteInput delay minutes
   * @return boolean
   */
  public boolean setDelay(int hourInput, int minuteInput) {
    try {
      if (Validator.validateTimeInts(hourInput, minuteInput) == false) {
        throw new IllegalArgumentException("Delay can not be less than 0");
      }
      this.delay = LocalTime.of(hourInput, minuteInput);
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  // ---- Methods ----

  /**
   * Returns a string array with the data from the object applicable for a table.
   * if track = -1, track is set to "Not set"
   *
   * @return String[] {departureTime, line, train number, destination, delay, track}
   */
  public String[] toTable() {
    StringBuilder sbTrack = new StringBuilder();
    if (this.track == -1) {
      sbTrack.append("Not set");
    } else {
      sbTrack.append(this.track);
    }

    String delay;
    if (this.delay.toString().equals("00:00")) {
      delay = "";
    } else {
      delay = this.delay.toString();
    }

    StringBuilder sbTrainNumber = new StringBuilder();
    sbTrainNumber.append(this.trainNumber);

    String[] data = {
        this.departureTime.toString(),
        this.line,
        sbTrainNumber.toString(),
        this.destination,
        delay,
        sbTrack.toString()};
    return data;
  }
}
