package edu.ntnu.stud.application;

// ---- Imports ----

import edu.ntnu.stud.utils.Validator;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/**
 * <p>This is the User Interface class.
 * Here every view is defined and run. It also contains all logic for button press and user
 * interaction with the application.
 * It contains two public methods, init() and start(), as well as some private helper
 * methods and ActionListeners for the buttons.</p>
 * <p>init() initializes the main window and a object of the DepartureRegister class.</p>
 * <p>start() updates the table with data from the register and makes the main window
 * visible to the user. </p>
 *
 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @since v0.1
 */
public class UserInterface {

  // ---- Fields ----
  private static final int defaultHour = 0;
  private static final int defaultMinute = 0;
  private static final String defaultHourString = "00";
  private static final String defualtMinuteString = "00";
  private static LocalTime currentTime = LocalTime.of(defaultHour, defaultMinute);
  // ---- Register ----
  private static DepartureRegister departureRegister;

  // ---- Fonts ----
  private static final Font h1FontBold = new Font("Calibri", Font.BOLD, 20);
  private static final Font h2FontBold = new Font("Calibri", Font.BOLD, 18);
  private static final Font h3FontBold = new Font("Calibri", Font.BOLD, 16);
  private static final Font h4FontBold = new Font("Calibri", Font.BOLD, 14);
  private static final Font h4FontItalic = new Font("Calibri", Font.ITALIC, 11);

  // ---- Dimensions ----
  private static final Dimension menuFieldSize = new Dimension(130, 30);
  private static final Dimension menuButtonSize = new Dimension(100, 30);
  private static final Dimension menuSectionSpacing = new Dimension(0, 20);

  private static final Dimension clockFieldSize = new Dimension(35, 30);
  private static final Dimension clockButtonSize = new Dimension(80, 30);
  private static final Dimension clockColonSize = new Dimension(5, 30);

  // ---- Main menu ----
  private static JFrame frame;
  private static JLabel header;
  private static JTable table;
  private static DefaultTableModel tableModel;

  // -- Clock --
  private static JPanel clock;
  private static JTextField hourField;
  private static JLabel colon;
  private static JTextField minuteField;
  private static JButton setTimeButton;

  // -- Menu --
  private static JPanel menu;
  private static JLabel menuHeader;
  private static JLabel editDepartureHeader;
  private static JButton addButton;
  private static JButton removeButton;
  private static JButton editButton;
  private static JLabel filtersHeader;
  private static JLabel trainNumberHeader;
  private static JTextField trainNumberField;
  private static JLabel destinationHeader;
  private static JTextField destinationField;
  private static JLabel departureTimeHeader;
  private static JTextField departureTimeField;
  private static JButton searchButton;
  private static JButton resetFiltersButton;

  // -- Action Listeners --
  private static ActionListener setTimeButtonActionListener;
  private static ActionListener addButtonActionListener;
  private static ActionListener removeButtonActionListener;
  private static ActionListener editButtonActionListener;
  private static ActionListener searchButtonActionListener;
  private static ActionListener resetFiltersButtonActionListener;

  // ---- Add departure window ----
  private static JFrame addDepartureFrame;
  private static JPanel addDeparturePanel;
  private static JLabel addDepartureHeader;
  private static JLabel addDepartureSubHeader;
  private static JLabel addDepartureTrainNumberHeader;
  private static JTextField addDepartureTrainNumberField;
  private static JLabel addDepartureDestinationHeader;
  private static JTextField addDepartureDestinationField;
  private static JLabel addDepartureDepartureTimeHeader;
  private static JTextField addDepartureDepartureTimeField;
  private static JLabel addDepartureLineHeader;
  private static JTextField addDepartureLineField;
  private static JLabel addDepartureTrackHeader;
  private static JTextField addDepartureTrackField;
  private static JLabel addDepartureDelayHeader;
  private static JTextField addDepartureDelayField;
  private static JButton submitDepartureButton;

  // -- Action Listeners --
  private static ActionListener submitDepartureButtonActionListener;

  // ---- Remove departure window ----
  private static JFrame removeDepartureFrame;
  private static JPanel removeDeparturePanel;
  private static JLabel removeDepartureHeader;
  private static JLabel removeDepartureSubHeader;
  private static JLabel removeDepartureTrainNumberHeader;
  private static JTextField removeDepartureTrainNumberField;
  private static JButton submitRemoveDepartureButton;

  // -- Action Listeners --
  private static ActionListener submitRemoveDepartureButtonActionListener;

  // ---- Edit departure window ----
  private static JFrame editDepartureFrame;
  private static JPanel editDeparturePanel;
  private static JLabel editDepartureWindowHeader;
  private static JLabel editDepartureSubHeader;
  private static JLabel editDepartureSearchTrainNumberHeader;
  private static JTextField editDepartureSearchTrainNumberField;
  private static JButton editDepartureSearchButton;
  private static JLabel editDepartureSearchTrainNumberResultLabel;
  private static JLabel editInputFieldsHeader;
  private static JLabel editDepartureTrackHeader;
  private static JTextField editDepartureTrackField;
  private static JLabel editDepartureDelayHeader;
  private static JTextField editDepartureDelayField;
  private static JButton editDepartureSubmitButton;

  // -- Action Listeners --
  private static ActionListener editDepartureSearchButtonActionListener;
  private static ActionListener editDepartureSubmitButtonActionListener;

  private static int trainNumberToBeEdited;

  /**
   * Private constructor to prevent instantiation.
   */
  private UserInterface() {
  }

  /**
   * Initializes the register of departures and sets up the user interface.
   */
  public static void init() {
    departureRegister = new DepartureRegister();
    setupMainWindow();
  }

  /**
   * Makes the user interface visible.
   */
  public static void start() {
    // ---- start frame ----
    addTestTableData();
    updateTable();
    frame.setVisible(true);
  }

  /**
   * <p>Sets up the main window of the user interface.
   * Also contains ActionListener for button objects in the main window.</p>
   * <p>setTimeButtonActionListener: Takes inn text from hourField and minuteField,
   * checks that it is within parameters and calls setTime methode to change the
   * global time perspective of the table data.</p>
   * <p>addButtonActionListener: Calls setupAddDepartureWindow() to set up the "add
   * departure" menu and makes it visible.</p>
   * <p>removeButtonActionListener: Calls setupRemoveDepartureWindow() to set up the
   * "remove departure" menu and makes it visible.</p>
   * <p>editButtonActionListener: Calls setupEditDepartureWindow() to set up the "edit
   * departure" menu and makes it visible.</p>
   * <p>searchButtonActionListener: Takes inn text from trainNumberField, destinationField
   * and departureTimeField. Checks that input is within parameters and calls
   * updateTable(String, String, String) to update the table with filter data.
   * The filter is dynamic, so you can choose to input in only one, two or all of the fields.
   * The logic then ignores filter parameters without input.</p>
   * <p>resetFiltersButtonActionListener: Calls updateTable() to update the table without
   * filter.</p>
   * <p> GitHub Copilot helped me generate the UI elements in this menu. I planned the layout
   * and created fields for elements, I also planned the code structure and copilot generated the
   * elements based on this. I created the logic in the actionListeners myself.</p>
   */
  private static void setupMainWindow() {
    // ---- init Frame ----
    frame = new JFrame();
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.setSize(1000, 600);
    frame.setLayout(null);
    frame.setResizable(false);

    // ---- init header ----
    header = new JLabel();
    header.setFont(h1FontBold);
    header.setBounds(150, 20, 150, 20);
    header.setText("Departures");
    frame.add(header);

    // ---- init clock ----
    clock = new JPanel();
    clock.setLayout(new BoxLayout(clock, BoxLayout.X_AXIS));
    clock.setBounds(825, 20, 160, 20);
    clock.setAlignmentX(Component.RIGHT_ALIGNMENT);

    // -- Hour text box --
    hourField = new JTextField(defaultHourString);
    hourField.setMaximumSize(clockFieldSize);
    clock.add(hourField);

    // -- Colon --
    colon = new JLabel(":");
    colon.setMaximumSize(clockColonSize);
    clock.add(colon);

    // -- Minute text box --
    minuteField = new JTextField(defualtMinuteString);
    minuteField.setMaximumSize(clockFieldSize);
    clock.add(minuteField);

    // -- Set time button --
    setTimeButton = new JButton("Set time");
    setTimeButton.setMaximumSize(clockButtonSize);
    clock.add(setTimeButton);

    // ---- add clock to frame ----
    frame.add(clock);

    // ---- init table ----
    String[] columnNames = {"Time", "Line", "Train Number", "Destination", "Delay", "Track"};
    tableModel = new DefaultTableModel(columnNames, 0);
    table = new JTable(tableModel);
    table.setShowGrid(false);
    table.setShowHorizontalLines(true);
    JScrollPane tableScrollPane = new JScrollPane(table);
    tableScrollPane.setBounds(150, 50, 830, 500);
    frame.add(tableScrollPane);

    // ---- init Menu ----
    menu = new JPanel();
    menu.setLayout(new BoxLayout(menu, BoxLayout.Y_AXIS));
    menu.setBounds(10, 50, 130, 500);
    menu.setAlignmentX(Component.CENTER_ALIGNMENT);

    // ---- Menu header ----
    menuHeader = new JLabel("Menu");
    menuHeader.setFont(h2FontBold);
    menuHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    menu.add(menuHeader);

    // ---- Spacing ----
    menu.add(Box.createRigidArea(menuSectionSpacing));

    // ---- Edit departure section ----
    editDepartureHeader = new JLabel("Edit departure");
    editDepartureHeader.setFont(h3FontBold);
    editDepartureHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    menu.add(editDepartureHeader);

    // -- Add button --
    addButton = new JButton("Add");
    addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    addButton.setMaximumSize(menuButtonSize);
    menu.add(addButton);

    // -- Remove button --
    removeButton = new JButton("Remove");
    removeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    removeButton.setMaximumSize(menuButtonSize);
    menu.add(removeButton);

    // -- Edit button --
    editButton = new JButton("Edit");
    editButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    editButton.setMaximumSize(menuButtonSize);
    menu.add(editButton);

    // ---- Spacing ----
    menu.add(Box.createRigidArea(menuSectionSpacing));

    // ---- Filters section ----
    // -- Header --
    filtersHeader = new JLabel("Filters");
    filtersHeader.setFont(h3FontBold);
    filtersHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    menu.add(filtersHeader);

    // -- Train number text field --
    trainNumberHeader = new JLabel("Train number");
    trainNumberHeader.setFont(h4FontItalic);
    trainNumberHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    menu.add(trainNumberHeader);

    trainNumberField = new JTextField();
    trainNumberField.setAlignmentX(Component.CENTER_ALIGNMENT);
    trainNumberField.setMaximumSize(menuFieldSize);
    menu.add(trainNumberField);

    // -- Destination text field --
    destinationHeader = new JLabel("Destination");
    destinationHeader.setFont(h4FontItalic);
    destinationHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    menu.add(destinationHeader);

    destinationField = new JTextField();
    destinationField.setAlignmentX(Component.CENTER_ALIGNMENT);
    destinationField.setMaximumSize(menuFieldSize);
    menu.add(destinationField);

    // -- Departure time text field --
    departureTimeHeader = new JLabel("Departure time (hh:mm)");
    departureTimeHeader.setFont(h4FontItalic);
    departureTimeHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    menu.add(departureTimeHeader);

    departureTimeField = new JTextField();
    departureTimeField.setAlignmentX(Component.CENTER_ALIGNMENT);
    departureTimeField.setMaximumSize(menuFieldSize);
    menu.add(departureTimeField);

    // -- Search button --
    searchButton = new JButton("Search");
    searchButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    searchButton.setMaximumSize(menuButtonSize);
    menu.add(searchButton);

    // -- Reset filters button --
    resetFiltersButton = new JButton("Reset");
    resetFiltersButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    resetFiltersButton.setMaximumSize(menuButtonSize);
    menu.add(resetFiltersButton);

    // ---- add menu to frame ----
    frame.add(menu);

    // ---- Action Listeners ----
    // -- Set time button --
    setTimeButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String hourInputString = hourField.getText();
          String minuteInputString = minuteField.getText();

          int hourInt = Integer.parseInt(hourInputString);
          int minuteInt = Integer.parseInt(minuteInputString);

          if (Validator.validateTimeInts(hourInt, minuteInt) == false) {
            throw new IllegalArgumentException("Time input not valid");
          }

          setTime(hourInt, minuteInt);
        } catch (NumberFormatException nfe) {
          setTime(defaultHour, defaultMinute);
          System.out.println(nfe.getMessage());
          System.out.println("Time set to default");
        } catch (IllegalArgumentException iae) {
          setTime(defaultHour, defaultHour);
          System.out.println(iae.getMessage());
          System.out.println("Time set to default");
        }
      }
    };

    // -- Add button --
    addButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        setupAddDepartureWindow();
        addDepartureFrame.setVisible(true);
      }
    };

    // -- Remove button --
    removeButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        setupRemoveDepartureWindow();
        removeDepartureFrame.setVisible(true);
      }
    };

    // -- Edit button --
    editButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        setupEditDepartureWindow();
        editDepartureFrame.setVisible(true);
      }
    };

    // -- Search button --
    searchButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String trainNumberInputString;
          String destinationInputString;
          String departureTimeInputString;

          try {
            trainNumberInputString = trainNumberField.getText();
            if (trainNumberInputString == null || trainNumberInputString.isEmpty()) {
              throw new NullPointerException("No trainNumber given");
            }
            if (Validator.validatePositiveIntString(trainNumberInputString) == false) {
              throw new NumberFormatException("Train number cannot be less than 1");
            }
          } catch (NullPointerException npe) {
            trainNumberInputString = null;
          }

          try {
            destinationInputString = destinationField.getText();
            if (destinationInputString == null || destinationInputString.isEmpty()) {
              throw new NullPointerException("No Destination given");
            }
          } catch (NullPointerException npe) {
            destinationInputString = null;
          }

          try {
            departureTimeInputString = departureTimeField.getText();
            if (departureTimeInputString == null || departureTimeInputString.isEmpty()) {
              throw new NullPointerException("No departureTime given");
            }
            if (Validator.validateTimeString(departureTimeInputString) == false) {
              throw new IllegalArgumentException("Time format invalid");
            }
          } catch (NullPointerException npe) {
            departureTimeInputString = null;
          }

          updateTable(trainNumberInputString, destinationInputString, departureTimeInputString);
        } catch (NumberFormatException nfe) {
          System.out.println(nfe.getMessage());
          System.out.println("Table cleared and not updated");
          clearTable();
        } catch (IllegalArgumentException iae) {
          System.out.println(iae.getMessage());
          System.out.println("Table cleared and not updated");
          clearTable();
        }
      }
    };

    // -- Reset filters button --
    resetFiltersButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        updateTable();
        trainNumberField.setText("");
        destinationField.setText("");
        departureTimeField.setText("");
      }
    };

    // ---- Add action listeners ----
    setTimeButton.addActionListener(setTimeButtonActionListener);
    addButton.addActionListener(addButtonActionListener);
    removeButton.addActionListener(removeButtonActionListener);
    editButton.addActionListener(editButtonActionListener);
    searchButton.addActionListener(searchButtonActionListener);
    resetFiltersButton.addActionListener(resetFiltersButtonActionListener);
  }

  // ---- Set up pop up windows ----

  /**
   * <p>
   * Sets up the add departure menu window.
   * </p>
   * <p>
   * submitDepartureButtonActionListener: Takes inn input from all fields,
   * validates that they are within parameters and then calls addTrainDeparture()
   * to add the new departure to register.
   * It then gives feedback to user and resets input fields.
   * </p>
   * <p>
   * GitHub Copilot helped me generate the UI elements in this menu. I planned the
   * layout and created fields for elements, I also planned the code structure and
   * copilot generated the elements based on this. I created the logic in the
   * actionListeners myself
   * </p>
   */
  private static void setupAddDepartureWindow() {
    // ---- Init add departure frame ----
    addDepartureFrame = new JFrame();
    addDepartureFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    addDepartureFrame.setSize(300, 450);
    addDepartureFrame.setLayout(null);
    addDepartureFrame.setResizable(false);

    // ---- Init panel ----
    addDeparturePanel = new JPanel();
    addDeparturePanel.setLayout(new BoxLayout(addDeparturePanel, BoxLayout.Y_AXIS));
    addDeparturePanel.setBounds(10, 10, 280, 430);
    addDeparturePanel.setAlignmentX(Component.CENTER_ALIGNMENT);

    // ---- Init header ----
    addDepartureHeader = new JLabel("Add departure");
    addDepartureHeader.setFont(h2FontBold);
    addDepartureHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureHeader);

    // ---- Init sub header ----
    addDepartureSubHeader = new JLabel("Enter the data");
    addDepartureSubHeader.setFont(h4FontBold);
    addDepartureSubHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureSubHeader);

    // ---- Init spacing ----
    addDeparturePanel.add(Box.createRigidArea(menuSectionSpacing));

    // ---- Init train number header ----
    addDepartureTrainNumberHeader = new JLabel("Train number");
    addDepartureTrainNumberHeader.setFont(h4FontItalic);
    addDepartureTrainNumberHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureTrainNumberHeader);

    // ---- Init train number field ----
    addDepartureTrainNumberField = new JTextField();
    addDepartureTrainNumberField.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDepartureTrainNumberField.setMaximumSize(menuFieldSize);
    addDeparturePanel.add(addDepartureTrainNumberField);

    // ---- Init destination header ----
    addDepartureDestinationHeader = new JLabel("Destination");
    addDepartureDestinationHeader.setFont(h4FontItalic);
    addDepartureDestinationHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureDestinationHeader);

    // ---- Init destination field ----
    addDepartureDestinationField = new JTextField();
    addDepartureDestinationField.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDepartureDestinationField.setMaximumSize(menuFieldSize);
    addDeparturePanel.add(addDepartureDestinationField);

    // ---- Init departure time header ----
    addDepartureDepartureTimeHeader = new JLabel("Departure time (hh:mm)");
    addDepartureDepartureTimeHeader.setFont(h4FontItalic);
    addDepartureDepartureTimeHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureDepartureTimeHeader);

    // ---- Init departure time field ----
    addDepartureDepartureTimeField = new JTextField();
    addDepartureDepartureTimeField.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDepartureDepartureTimeField.setMaximumSize(menuFieldSize);
    addDeparturePanel.add(addDepartureDepartureTimeField);

    // ---- Init line header ----
    addDepartureLineHeader = new JLabel("Line");
    addDepartureLineHeader.setFont(h4FontItalic);
    addDepartureLineHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureLineHeader);

    // ---- Init line field ----
    addDepartureLineField = new JTextField();
    addDepartureLineField.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDepartureLineField.setMaximumSize(menuFieldSize);
    addDeparturePanel.add(addDepartureLineField);

    // ---- Init track header ----
    addDepartureTrackHeader = new JLabel("Track");
    addDepartureTrackHeader.setFont(h4FontItalic);
    addDepartureTrackHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureTrackHeader);

    // ---- Init track field ----
    addDepartureTrackField = new JTextField();
    addDepartureTrackField.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDepartureTrackField.setMaximumSize(menuFieldSize);
    addDeparturePanel.add(addDepartureTrackField);

    // ---- Init delay header ----
    addDepartureDelayHeader = new JLabel("Delay (hh:mm)");
    addDepartureDelayHeader.setFont(h4FontItalic);
    addDepartureDelayHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDeparturePanel.add(addDepartureDelayHeader);

    // ---- Init delay field ----
    addDepartureDelayField = new JTextField();
    addDepartureDelayField.setAlignmentX(Component.CENTER_ALIGNMENT);
    addDepartureDelayField.setMaximumSize(menuFieldSize);
    addDeparturePanel.add(addDepartureDelayField);

    // ---- Init submit button ----
    submitDepartureButton = new JButton("Submit");
    submitDepartureButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    submitDepartureButton.setMaximumSize(menuButtonSize);
    addDeparturePanel.add(submitDepartureButton);

    // ---- Add panel to frame ----
    addDepartureFrame.add(addDeparturePanel);

    // ---- Action listeners ----
    submitDepartureButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String departureTimeInput = addDepartureDepartureTimeField.getText();
          if (Validator.validateTimeString(departureTimeInput) == false) {
            throw new IllegalArgumentException("Departure time input invalid");
          }

          String trainNumberInput = addDepartureTrainNumberField.getText();
          if (Validator.validatePositiveIntString(trainNumberInput) == false) {
            throw new IllegalArgumentException("Train number input invalid");
          }

          String trackInput = addDepartureTrackField.getText();
          if (Validator.validateTrackString(trackInput) == false) {
            throw new IllegalArgumentException("Track input invalid");
          }

          String delayInput = addDepartureDelayField.getText();
          if (Validator.validateTimeString(delayInput) == false) {
            throw new IllegalArgumentException("Delay input invalid");
          }

          int trainNumber = Integer.parseInt(trainNumberInput);
          if (checkDepartureExistence(trainNumber) == true) {
            throw new IllegalArgumentException("This train number already exists");
          }

          String destination = addDepartureDestinationField.getText();
          if (Validator.validateNotEmptyString(destination) == false) {
            throw new IllegalArgumentException("Destination can not be empty");
          }

          String lineInput = addDepartureLineField.getText();
          if (Validator.validateNotEmptyString(lineInput) == false) {
            throw new IllegalArgumentException("Line can not be empty");
          }

          LocalTime departureTime = LocalTime.parse(departureTimeInput);
          int track = Integer.parseInt(trackInput);

          String[] delayParts = delayInput.split(":");
          LocalTime delay = LocalTime.of(Integer.parseInt(delayParts[0]),
                  Integer.parseInt(delayParts[1]));

          if (departureRegister.addTrainDeparture(
                  departureTime,
                  lineInput,
                  trainNumber,
                  destination,
                  track,
                  delay)) {
            addDepartureSubHeader.setText("Departure successfully added");
          }

          addDepartureTrainNumberField.setText("");
          addDepartureDestinationField.setText("");
          addDepartureDepartureTimeField.setText("");
          addDepartureLineField.setText("");
          addDepartureTrackField.setText("");
          addDepartureDelayField.setText("");

          updateTable();
        } catch (IllegalArgumentException iae) {
          System.out.println(iae.getMessage());
          addDepartureSubHeader.setText(iae.getMessage());
        } catch (DateTimeException dte) {
          System.out.println(dte.getMessage());
          addDepartureSubHeader.setText("Input invalid, try again");
        } catch (NullPointerException npe) {
          System.out.println(npe.getMessage());
          addDepartureSubHeader.setText("Input invalid, try again");
        } catch (ArrayIndexOutOfBoundsException aioobe) {
          System.out.println(aioobe.getMessage());
          addDepartureSubHeader.setText("Input invalid, try again");
        }
      }
    };

    // ---- Add action listeners ----
    submitDepartureButton.addActionListener(submitDepartureButtonActionListener);
  }

  /**
   * <p>
   * Sets up the Remove Departure window.
   * </p>
   * <p>
   * submitRemoveDepartureButtonActionListener: Takes inn input from
   * trainNumberField, validates that it is within parameters and then calls
   * removeTrainDeparture() to remove the departure from register.
   * </p>
   * <p>
   * GitHub Copilot helped me generate the UI elements in this menu. I planned the
   * layout and created fields for elements, I also planned the code structure and
   * copilot generated the elements based on this. I created the logic in the
   * actionListeners myself
   * </p>
   */
  private static void setupRemoveDepartureWindow() {
    // ---- Init remove departure frame ----
    removeDepartureFrame = new JFrame();
    removeDepartureFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    removeDepartureFrame.setSize(300, 200);
    removeDepartureFrame.setLayout(null);
    removeDepartureFrame.setResizable(false);

    // ---- Init panel ----
    removeDeparturePanel = new JPanel();
    removeDeparturePanel.setLayout(new BoxLayout(removeDeparturePanel, BoxLayout.Y_AXIS));
    removeDeparturePanel.setBounds(10, 10, 280, 190);
    removeDeparturePanel.setAlignmentX(Component.CENTER_ALIGNMENT);

    // ---- Init header ----
    removeDepartureHeader = new JLabel("Remove departure");
    removeDepartureHeader.setFont(h2FontBold);
    removeDepartureHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    removeDeparturePanel.add(removeDepartureHeader);

    // ---- Init sub header ----
    removeDepartureSubHeader = new JLabel("Enter the train number");
    removeDepartureSubHeader.setFont(h4FontBold);
    removeDepartureSubHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    removeDeparturePanel.add(removeDepartureSubHeader);

    // ---- Init spacing ----
    removeDeparturePanel.add(Box.createRigidArea(menuSectionSpacing));

    // ---- Init train number header ----
    removeDepartureTrainNumberHeader = new JLabel("Train number");
    removeDepartureTrainNumberHeader.setFont(h4FontItalic);
    removeDepartureTrainNumberHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    removeDeparturePanel.add(removeDepartureTrainNumberHeader);

    // ---- Init train number field ----
    removeDepartureTrainNumberField = new JTextField();
    removeDepartureTrainNumberField.setAlignmentX(Component.CENTER_ALIGNMENT);
    removeDepartureTrainNumberField.setMaximumSize(menuFieldSize);
    removeDeparturePanel.add(removeDepartureTrainNumberField);

    // ---- Init submit button ----
    submitRemoveDepartureButton = new JButton("Submit");
    submitRemoveDepartureButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    submitRemoveDepartureButton.setMaximumSize(menuButtonSize);
    removeDeparturePanel.add(submitRemoveDepartureButton);

    // ---- Add panel to frame ----
    removeDepartureFrame.add(removeDeparturePanel);

    // ---- Action listeners ----
    submitRemoveDepartureButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String trainNumberInput = removeDepartureTrainNumberField.getText();
          if (Validator.validatePositiveIntString(trainNumberInput) == false) {
            throw new IllegalArgumentException("Train number input invalid");
          }

          int trainNumber = Integer.parseInt(trainNumberInput);
          if (departureRegister.removeTrainDeparture(trainNumber)) {
            removeDepartureSubHeader.setText("Departure successfully removed");
          } else {
            removeDepartureSubHeader.setText("Departure not found");
          }

          removeDepartureTrainNumberField.setText("");

          updateTable();
        } catch (IllegalArgumentException iae) {
          System.out.println(iae.getMessage());
          removeDepartureSubHeader.setText("Input invalid, try again");
        }
      }
    };

    // ---- Add action listeners ----
    submitRemoveDepartureButton.addActionListener(submitRemoveDepartureButtonActionListener);
  }

  /**
   * Sets up the Edit Departure window.
   * Action handler for search button and submit button
   * <p>
   * editDepartureSearchButtonActionListener: Takes inn input from
   * editDepartureSearchTrainNumberField, validates that it is within parameters
   * and then calls checkDepartureExistence() to check if the departure exists.
   * If it exists, it sets the global trainNumberToBeEdited variable to the train
   * number and gives feedback to user.
   * </p>
   * <p>
   * editDepartureSubmitButtonActionListener: Takes inn input from
   * editDepartureTrackField and editDepartureDelayField, validates that they are
   * within parameters and then calls editTrainDeparture() with
   * trainNumberToBeEdited from previous ActionListener to edit the departure.
   * </p>
   * <p>
   * GitHub Copilot helped me generate the UI elements in this menu. I planned the
   * layout and created fields for elements, I also planned the code structure and
   * copilot generated the elements based on this. I created the logic in the
   * actionListeners myself
   * </p>
   */
  private static void setupEditDepartureWindow() {
    // ---- Init edit departure frame ----
    editDepartureFrame = new JFrame();
    editDepartureFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    editDepartureFrame.setSize(300, 450);
    editDepartureFrame.setLayout(null);
    editDepartureFrame.setResizable(false);

    // ---- Init panel ----
    editDeparturePanel = new JPanel();
    editDeparturePanel.setLayout(new BoxLayout(editDeparturePanel, BoxLayout.Y_AXIS));
    editDeparturePanel.setBounds(10, 10, 280, 430);
    editDeparturePanel.setAlignmentX(Component.CENTER_ALIGNMENT);

    // ---- Init header ----
    editDepartureWindowHeader = new JLabel("Edit departure");
    editDepartureWindowHeader.setFont(h2FontBold);
    editDepartureWindowHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDeparturePanel.add(editDepartureWindowHeader);

    // ---- Init sub header ----
    editDepartureSubHeader = new JLabel("Enter the train number");
    editDepartureSubHeader.setFont(h4FontBold);
    editDepartureSubHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDeparturePanel.add(editDepartureSubHeader);

    // ---- Init spacing ----
    editDeparturePanel.add(Box.createRigidArea(menuSectionSpacing));

    // ---- Init search header ----
    editDepartureSearchTrainNumberHeader = new JLabel("Search for departure");
    editDepartureSearchTrainNumberHeader.setFont(h3FontBold);
    editDepartureSearchTrainNumberHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDeparturePanel.add(editDepartureSearchTrainNumberHeader);

    // ---- Init search train number field ----
    editDepartureSearchTrainNumberField = new JTextField();
    editDepartureSearchTrainNumberField.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDepartureSearchTrainNumberField.setMaximumSize(menuFieldSize);
    editDeparturePanel.add(editDepartureSearchTrainNumberField);

    // ---- Init search button ----
    editDepartureSearchButton = new JButton("Search");
    editDepartureSearchButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDepartureSearchButton.setMaximumSize(menuButtonSize);
    editDeparturePanel.add(editDepartureSearchButton);

    // ---- Init search result label ----
    editDepartureSearchTrainNumberResultLabel = new JLabel();
    editDepartureSearchTrainNumberResultLabel.setFont(h4FontItalic);
    editDepartureSearchTrainNumberResultLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDeparturePanel.add(editDepartureSearchTrainNumberResultLabel);

    // ---- Init spacing ----
    editDeparturePanel.add(Box.createRigidArea(menuSectionSpacing));

    // ---- Init edit fields header ----
    editInputFieldsHeader = new JLabel("Input new data");
    editInputFieldsHeader.setFont(h3FontBold);
    editInputFieldsHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDeparturePanel.add(editInputFieldsHeader);

    // ---- Init track header ----
    editDepartureTrackHeader = new JLabel("Track");
    editDepartureTrackHeader.setFont(h4FontItalic);
    editDepartureTrackHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDeparturePanel.add(editDepartureTrackHeader);

    // ---- Init track field ----
    editDepartureTrackField = new JTextField();
    editDepartureTrackField.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDepartureTrackField.setMaximumSize(menuFieldSize);
    editDeparturePanel.add(editDepartureTrackField);

    // ---- Init delay header ----
    editDepartureDelayHeader = new JLabel("Delay (hh:mm)");
    editDepartureDelayHeader.setFont(h4FontItalic);
    editDepartureDelayHeader.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDeparturePanel.add(editDepartureDelayHeader);

    // ---- Init delay field ----
    editDepartureDelayField = new JTextField();
    editDepartureDelayField.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDepartureDelayField.setMaximumSize(menuFieldSize);
    editDeparturePanel.add(editDepartureDelayField);

    // ---- Init submit button ----
    editDepartureSubmitButton = new JButton("Submit");
    editDepartureSubmitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    editDepartureSubmitButton.setMaximumSize(menuButtonSize);
    editDeparturePanel.add(editDepartureSubmitButton);

    // ---- Add panel to frame ----
    editDepartureFrame.add(editDeparturePanel);

    // ---- Action listeners ----
    editDepartureSearchButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String trainNumberInput = editDepartureSearchTrainNumberField.getText();
          if (Validator.validatePositiveIntString(trainNumberInput) == false) {
            throw new NumberFormatException("Train number input invalid");
          }

          int trainNumber = Integer.parseInt(trainNumberInput);
          if (checkDepartureExistence(trainNumber) == false) {
            throw new IllegalArgumentException("Departure not found");
          }

          trainNumberToBeEdited = trainNumber;
          editDepartureSearchTrainNumberResultLabel.setText("Editing train number: " + trainNumber);

          editDepartureSearchTrainNumberField.setText("");

        } catch (NumberFormatException nfe) {
          System.out.println(nfe.getMessage());
          editDepartureSearchTrainNumberResultLabel.setText(nfe.getMessage());
        } catch (IllegalArgumentException iae) {
          System.out.println(iae.getMessage());
          editDepartureSearchTrainNumberResultLabel.setText(iae.getMessage());
        }
      }
    };

    editDepartureSubmitButtonActionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          if (trainNumberToBeEdited == 0) {
            throw new IllegalArgumentException("No departure selected");
          }
          if (checkDepartureExistence(trainNumberToBeEdited) == false) {
            throw new IllegalArgumentException("Departure not found");
          }

          String trackInput = editDepartureTrackField.getText();
          String delayInput = editDepartureDelayField.getText();
          if (Validator.validateTrackString(trackInput) == false) {
            throw new IllegalArgumentException("Track input invalid");
          }
          if (Validator.validateTimeString(delayInput) == false) {
            throw new IllegalArgumentException("Delay input invalid");
          }

          int track = Integer.parseInt(trackInput);
          String[] delayParts = delayInput.split(":");
          LocalTime delay = LocalTime.of(Integer.parseInt(delayParts[0]),
                  Integer.parseInt(delayParts[1]));

          if (departureRegister.editDeparture(trainNumberToBeEdited, track, delay)) {
            editInputFieldsHeader.setText("Departure successfully edited");
          }

          trainNumberToBeEdited = 0;
          editDepartureTrackField.setText("");
          editDepartureDelayField.setText("");
          editDepartureSearchTrainNumberResultLabel.setText("");

          updateTable();
        } catch (IllegalArgumentException iae) {
          System.out.println(iae.getMessage());
          editInputFieldsHeader.setText(iae.getMessage());
        }
      }
    };

    // ---- Add action listeners ----
    editDepartureSearchButton.addActionListener(editDepartureSearchButtonActionListener);
    editDepartureSubmitButton.addActionListener(editDepartureSubmitButtonActionListener);

  }

  // ---- Private helper methods ----

  /**
   * Sets the time of the clock to the given time.
   * This time will change the global time perspective of the table data.
   * If the given time is out of bounds, the time is set to 00:00.
   *
   * @param hour   format: (hh) or (h)
   * @param minute format: (mm) or (m)
   */
  private static void setTime(int hour, int minute) {
    try {
      currentTime = LocalTime.of(hour, minute);

      if (hour < 10) {
        hourField.setText("0" + hour);
      } else {
        hourField.setText(Integer.toString(hour));
      }

      if (minute < 10) {
        minuteField.setText("0" + minute);
      } else {
        minuteField.setText(Integer.toString(minute));
      }

      updateTable();
    } catch (DateTimeException dte) {
      currentTime = LocalTime.of(defaultHour, defaultMinute);
      hourField.setText(defaultHourString);
      minuteField.setText(defualtMinuteString);
      System.out.println(dte.getMessage());
      System.out.println("Time set to 00:00");
    }
  }

  /**
   * Updates the table with the current data from the register.
   * Time frame is set to {currentTime} (set by user) - 23:59.
   */
  private static void updateTable() {
    try {
      clearTable();

      ArrayList<String[]> tableDataList =
              departureRegister.getDepartureTableDataWithinTimeFrame(
                      currentTime, LocalTime.of(23, 59));
      for (String[] tableData : tableDataList) {
        tableModel.addRow(tableData);
      }
    } catch (NullPointerException npe) {
      System.out.println(npe.getMessage());
      System.out.println("Table not updated");
    } catch (DateTimeException dte) {
      System.out.println(dte.getMessage());
      System.out.println("Table not updated");
    }
  }

  /**
   * Updates the table with departures that matches the given parameters.
   * All parameters are dynamic, if a parameter is set to null, that parameter is
   * ignored in the filter search.
   * Overloaded version of updateTable().
   *
   * @param trainNumberString   format: String of positive number
   * @param destinationString   format: String
   * @param departureTimeString must be in format hh:mm
   */
  private static void updateTable(
          String trainNumberString,
          String destinationString,
          String departureTimeString) {
    try {
      if (departureTimeString != null
              && Validator.validateTimeString(departureTimeString) == false) {
        throw new IllegalArgumentException("Time format invalid");
      }
      if (trainNumberString != null
              && Validator.validatePositiveIntString(trainNumberString) == false) {
        throw new IllegalArgumentException("Train number can not be less than 1");
      }

      clearTable();

      ArrayList<String[]> tableDataList =
              departureRegister.getDepartureTableDataWithinTimeFrame(
                      currentTime, LocalTime.of(23, 59));
      tableDataList.stream()
              .filter(tableData -> trainNumberString == null
                      || tableData[2].equals(trainNumberString))
              .filter(tableData -> destinationString == null
                      || tableData[3].equals(destinationString))
              .filter(tableData -> departureTimeString == null
                      || tableData[0].equals(departureTimeString))
              .forEach(tableData -> tableModel.addRow(tableData));
    } catch (NullPointerException npe) {
      System.out.println(npe.getMessage());
      System.out.println("Table cleared and not updated");
      clearTable();
    } catch (DateTimeException dte) {
      System.out.println(dte.getMessage());
      System.out.println("Table clears and not updated");
      clearTable();
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      System.out.println("Table cleared and not updated");
      clearTable();
    }
  }

  // ---- Private helper methods ----

  /**
   * Clears the table of all data.
   */
  private static void clearTable() {
    tableModel.setRowCount(0);
  }

  /**
   * Returns true if the given departure exists in the register.
   *
   * @param trainNumber train number
   * @return boolean
   */
  private static boolean checkDepartureExistence(int trainNumber) {
    return departureRegister.checkDepartureExistence(trainNumber);
  }

  // ---- Test data ----
  private static void addTestTableData() {
    departureRegister.addTrainDeparture(LocalTime.of(10, 30), "L1",
            1, "Oslo", -1, LocalTime.of(0, 0));
    departureRegister.addTrainDeparture(LocalTime.of(11, 30), "L1",
            2, "Oslo", 2, LocalTime.of(0, 0));
    departureRegister.addTrainDeparture(LocalTime.of(12, 30), "L2",
            3, "Drammen", 2, LocalTime.of(0, 0));
    departureRegister.addTrainDeparture(LocalTime.of(13, 30), "L1",
            4, "Kongsberg", 3, LocalTime.of(0, 0));
    departureRegister.addTrainDeparture(LocalTime.of(14, 30), "L1",
            5, "Kongsberg", -1, LocalTime.of(0, 0));
  }
}