package edu.ntnu.stud.utils;

/**
 * This class contains static methods for validating user input.
 * Not to be instantiated.
 *
 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @since v0.2
 */
public class Validator {

  /**
   * Private constructor to prevent instantiation.
   */
  private Validator() {
  }

  /**
   * Returns true if the given time input is valid.
   * Valid inputs using are hh:mm where hh is between 00 and 23 and mm is between 00 and 59.
   * 
   * <p>GitHub Copilot helped me design the regex string in this methode.<p/>

   * @param timeInput time in type string to validate
   * @return boolean
   */
  public static boolean validateTimeString(String timeInput) {
    String regex = "^([01]?[0-9]|2[0-3]):[0-5][0-9]$";
    return timeInput.matches(regex);
  }

  /**
   * Returns true if the given train number input is valid.
   * Valid input is a positive number.
   *
   * @param positiveNumberInput number in type string to be validated
   * @return boolean
   */
  public static boolean validatePositiveIntString(String positiveNumberInput) {
    try {
      int positiveNumber = Integer.parseInt(positiveNumberInput);
      if (positiveNumber < 1) {
        throw new IllegalArgumentException("Train number can not be less than 1");
      }
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Returns true if the given track input is valid.
   * Valid inputs are positive numbers and -1. -1 is used to
   * indicate that the departure has no track.
   *
   * @param trackInput track in type string to be validated
   * @return boolean
   */
  public static boolean validateTrackString(String trackInput) {
    try {
      int track = Integer.parseInt(trackInput);
      if (track < 1 && track != -1) {
        throw new IllegalArgumentException("Track number must be positive or -1");
      }
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Returns true if the given string input is not empty.
   *
   * @param input String to be validated
   * @return boolean
   */
  public static boolean validateNotEmptyString(String input) {
    try {
      if (input.isEmpty()) {
        throw new IllegalArgumentException("Input can not be empty");
      }
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Returns true if the given int input is not less than 1.
   *
   * @param positiveIntInput int to be validated
   * @return boolean
   */
  public static boolean validatePositiveInt(int positiveIntInput) {
    try {
      if (positiveIntInput < 1) {
        throw new IllegalArgumentException("Input can not be less than 1");
      }
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Returns true if the given track input is valid.
   * Valid inputs are positive numbers and -1. -1 is
   * used to indicate that the departure has no track.
   *
   * @param trackInput track input to be validated
   * @return boolean
   */
  public static boolean validateTrackInt(int trackInput) {
    try {
      if (trackInput < 1 && trackInput != -1) {
        throw new IllegalArgumentException("Track number must be positive or -1");
      }
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Returns true if the given time input is valid.
   * Valid inputs using are hh:mm where hh is between 00 and 23 and mm is between 00 and 59.
   *
   * @param hour hour int to be validated
   * @param minute  minute int to be validated
   * @return boolean
   */
  public static boolean validateTimeInts(int hour, int minute) {
    try {
      if (hour < 0 || hour > 23) {
        throw new IllegalArgumentException("Hour must be between 0 and 23");
      }
      if (minute < 0 || minute > 59) {
        throw new IllegalArgumentException("Minute must be between 0 and 59");
      }
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }
}
