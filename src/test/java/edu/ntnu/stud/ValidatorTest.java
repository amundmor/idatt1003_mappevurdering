package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.stud.utils.Validator;

/**
 * Class of test methods for the Validator class.
 *
 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @see Validator
 * @since v0.2
 */
public class ValidatorTest {
  @Nested
  @DisplayName("Collection of positive outcome tests for Validator")
  class PositiveValidatorTests {

    @Test
    @DisplayName("validateTimeString() returns true on valid time string")
    public void validateTimeStringReturnsTrueOnValidTimeString() {
      try {
        String testStringMin = "0:00";
        String testStringMax = "23:59";

        boolean resultBooleanMin = Validator.validateTimeString(testStringMin);
        boolean resultBooleanMax = Validator.validateTimeString(testStringMax);

        assertTrue(resultBooleanMin);
        assertTrue(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validatePositiveIntString() returns true on valid positive int in String type")
    public void validatePositiveIntStringReturnPositiveOnValidIntInStringType() {
      try {
        String testStringMin = "1";
        String testStringMax = "100";

        boolean resultBooleanMin = Validator.validatePositiveIntString(testStringMin);
        boolean resultBooleanMax = Validator.validatePositiveIntString(testStringMax);

        assertTrue(resultBooleanMin);
        assertTrue(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateTrackString() returns true on valid track input format")
    public void validateTrackStringReturnsTrueOnValidTrackInputFormat() {
      try {
        String testStringMinusOne = "-1";
        String testStringOne = "1";
        String testStringMax = "100";

        boolean resultBooleanMinusOne = Validator.validateTrackString(testStringMinusOne);
        boolean resultBooleanOne = Validator.validateTrackString(testStringOne);
        boolean resultBooleanMax = Validator.validateTrackString(testStringMax);

        assertTrue(resultBooleanMinusOne);
        assertTrue(resultBooleanOne);
        assertTrue(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateNotEmptyString() returns true on not empty string")
    public void validateNotEmptyStringReturnsTrueOnNotEmptyString() {
      try {
        String testString = "test1";

        boolean resultBoolean = Validator.validateNotEmptyString(testString);

        assertTrue(resultBoolean);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validatePositiveInt() Returns true on positive int")
    public void validatePositiveIntReturnsTrueOnPositiveInt() {
      try {
        int testIntMin = 1;
        int testIntMax = 1;

        boolean resultBooleanMin = Validator.validatePositiveInt(testIntMin);
        boolean resultBooleanMax = Validator.validatePositiveInt(testIntMax);

        assertTrue(resultBooleanMin);
        assertTrue(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateTrackInt() returns true on valid track int")
    public void validateTrackIntReturnsTrueOnValidTrackInt() {
      try {
        int testTrackMinusOne = -1;
        int testTrackOne = 1;
        int testTrackMax = 100;

        boolean resultBooleanMinusOne = Validator.validateTrackInt(testTrackMinusOne);
        boolean resultBooleanOne = Validator.validateTrackInt(testTrackOne);
        boolean resultBooleanMax = Validator.validateTrackInt(testTrackMax);

        assertTrue(resultBooleanMinusOne);
        assertTrue(resultBooleanOne);
        assertTrue(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateTimeInts() returns true on valid time ints")
    public void validateTimeIntsReturnsTrueOnValidTimeInts() {
      try {
        int testHourMin = 0;
        int testHourMax = 23;
        int testMinuteMin = 0;
        int testMinuteMax = 59;

        boolean resultBooleanMin = Validator.validateTimeInts(testHourMin, testMinuteMin);
        boolean resultBooleanMax = Validator.validateTimeInts(testHourMax, testMinuteMax);

        assertTrue(resultBooleanMin);
        assertTrue(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Collection of negative outcome tests for Validator")
  class NegativeValidatorTests {

    @Test
    @DisplayName("validateTimeString() returns false on invalid time string")
    public void validateTimeStringReturnsFalseOnInvalidTimeString() {
      try {
        String testStringMin = "0-1";
        String testStringMax = "24:01";
        String testStringOther = "test";

        boolean resultBooleanMin = Validator.validateTimeString(testStringMin);
        boolean resultBooleanMax = Validator.validateTimeString(testStringMax);
        boolean resultBooleanOther = Validator.validateTimeString(testStringOther);

        assertFalse(resultBooleanMin);
        assertFalse(resultBooleanMax);
        assertFalse(resultBooleanOther);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validatePositiveIntString() returns false on invalid int String")
    public void validatePositiveIntStringReturnsFalseOnInvalidIntString() {
      try {
        String testStringMin = "-1";
        String testStringMax = "-100";
        String testStringOther = "test";

        boolean resultBooleanMin = Validator.validatePositiveIntString(testStringMin);
        boolean resultBooleanMax = Validator.validatePositiveIntString(testStringMax);
        boolean resultBooleanOther = Validator.validatePositiveIntString(testStringOther);

        assertFalse(resultBooleanMin);
        assertFalse(resultBooleanMax);
        assertFalse(resultBooleanOther);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateTrackString() returns false on invalid track String")
    public void validateTrackStringReturnsFalseOnInvalidTrackString() {
      try {
        String testStringMin = "-2";
        String testStringMax = "-100";
        String testStringOther = "test";

        boolean resultBooleanMin = Validator.validateTrackString(testStringMin);
        boolean resultBooleanMax = Validator.validateTrackString(testStringMax);
        boolean resultBooleanOther = Validator.validateTrackString(testStringOther);

        assertFalse(resultBooleanMin);
        assertFalse(resultBooleanMax);
        assertFalse(resultBooleanOther);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateNotEmptyString() returns false on empty string input")
    public void validateNotEmptyStringReturnsFalseOnEmptyString() {
      try {
        String testString = "";

        boolean resultBoolean = Validator.validateNotEmptyString(testString);

        assertFalse(resultBoolean);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validatePositiveInt() returns false on negative int")
    public void validatePositiveIntReturnsFalseOnNegativeInt() {
      try {
        int testIntMin = -1;
        int testIntMax = -100;

        boolean resultBooleanMin = Validator.validatePositiveInt(testIntMin);
        boolean resultBooleanMax = Validator.validatePositiveInt(testIntMax);

        assertFalse(resultBooleanMin);
        assertFalse(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateTrackInt() returns false on int less than minus one")
    public void validateTrackIntReturnsFalseOnIntLessThanMinusOne() {
      try {
        int testIntMin = -2;
        int testIntMax = -100;

        boolean resultBooleanMin = Validator.validateTrackInt(testIntMin);
        boolean resultBooleanMax = Validator.validateTrackInt(testIntMax);

        assertFalse(resultBooleanMin);
        assertFalse(resultBooleanMax);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("validateTimeInts() returns false on invalid time ints")
    public void validateTimeIntsReturnsFalseOnInvalidTimeInts() {
      try {
        int testHourMax = 24;
        int testHourMin = -1;
        int testMinuteMax = 60;
        int testMinuteMin = -1;

        boolean resultBooleanMax = Validator.validateTimeInts(testHourMax, testMinuteMax);
        boolean resultBooleanMin = Validator.validateTimeInts(testHourMin, testMinuteMin);

        assertFalse(resultBooleanMax);
        assertFalse(resultBooleanMin);
      } catch (Exception e) {
        fail("Exception thrown: " + e.getMessage());
      }
    }
  }
}