package edu.ntnu.stud.application;

import edu.ntnu.stud.utils.Validator;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>The DepartureRegister class represents a register of train departures.
 * It stores a list of TrainDeparture objects in an ArrayList.
 * This class also contains methods that interacts with this register and its data.</p>
 *
 * @author Amund Mørk, amundmor@stud.ntnu.no, 10086
 * @version v1.0
 * @since v0.2
 */
public class DepartureRegister {
  // ---- Fields ----
  private ArrayList<TrainDeparture> departureRegister;

  // ---- Constructors ----

  /**
   * Default constructor.
   * Initializes a DepartureRegister object with an empty ArrayList of TrainDepartures.
   */
  public DepartureRegister() {
    this.departureRegister = new ArrayList<>();
  }

  /**
   * Alternative constructor.
   * Initializes a DepartureRegister object with an existing ArrayList as a parameter.
   *
   * @param departureRegister the ArrayList of TrainDeparture objects
   *                                     to initialize the DepartureRegister with.
   */
  public DepartureRegister(ArrayList<TrainDeparture> departureRegister) {
    this.departureRegister = departureRegister;
  }

  // ---- Getters ----
  public ArrayList<TrainDeparture> getRegisterArray() {
    return this.departureRegister;
  }

  // ---- Methods ----

  /**
   * Adds a new TrainDeparture.
   * Validates trainNumber and track before adding.
   *
   * @param departureTime Trains departure time
   * @param line Trains line
   * @param trainNumber Train number
   * @param destination Trains destination
   * @param track Track number train is arriving on
   * @param delay Trains potential delay
   * @return boolean, true if methode was successfully
   */
  public boolean addTrainDeparture(
          LocalTime departureTime,
          String line, int trainNumber,
          String destination,
          int track,
          LocalTime delay) {
    try {
      if (Validator.validatePositiveInt(trainNumber) == false) {
        throw new IllegalArgumentException("Train number must be a positive number.");
      }
      if (checkDepartureExistence(trainNumber) == true) {
        throw new IllegalArgumentException("Train departure already exists.");
      }
      if (Validator.validateTrackInt(track) == false) {
        throw new IllegalArgumentException("Track number must be a positive number or -1.");
      }

      TrainDeparture tempTrainDeparture = new TrainDeparture(
              departureTime,
              line,
              trainNumber,
              destination,
              track,
              delay);
      if (departureRegister.add(tempTrainDeparture)) {
        sortRegister();
        return true;
      }
      return false;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Removes TrainDeparture with given trainNumber if TrainDeparture is present in register.
   *
   * @param trainNumber type int
   * @return boolean, if removal was successfull it returns true, if not it returns false
   */
  public boolean removeTrainDeparture(int trainNumber) {
    try {
      TrainDeparture tempTrainDeparture = getDepartureByTrainNumber(trainNumber);
      return departureRegister.remove(tempTrainDeparture);
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Returns TrainDeparture object with the given train number.
   * Throws IllegalArgumentException if no TrainDeparture with the given train number is found.
   *
   * @param trainNumber Train number
   * @return TrainDeparture
   * @throws IllegalArgumentException Thrown if no departure is found
   */
  public TrainDeparture getDepartureByTrainNumber(int trainNumber) throws IllegalArgumentException {
    sortRegister();
    return getRegisterArray().stream()
            .filter(trainDeparture -> trainNumber == trainDeparture.getTrainNumber())
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Train departure not found."));
  }

  /**
   * Returns an ArrayList of TrainDeparture objects with the given destination.
   *
   * @param destination the destination to search for
   * @return ArrayList of TrainDeparture objects
   */
  public ArrayList<TrainDeparture> getDeparturesGoingToDestination(String destination) {
    sortRegister();
    return getRegisterArray().stream()
            .filter(trainDeparture -> destination.equals(trainDeparture.getDestination()))
            .collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * Returns an ArrayList of String lists containing the data of
   * all TrainDeparture objects in the register.
   *
   * @return ArrayList of String List objects
   */
  public ArrayList<String[]> getDepartureTableData() {
    sortRegister();
    ArrayList<String[]> departureTableData = new ArrayList<>();
    for (TrainDeparture departure : departureRegister) {
      departureTableData.add(departure.toTable());
    }
    return departureTableData;
  }

  /**
   * Returns an ArrayList of String arrays containing the data of all
   * TrainDeparture objects in the register within the given timeframe.
   * If given timeframe out of bounds, it returns the full table.
   *
   * @param startTime Start of time frame
   * @param endTime   End of time frame
   * @return ArrayList of String List objects
   */
  public ArrayList<String[]> getDepartureTableDataWithinTimeFrame(
          LocalTime startTime,
          LocalTime endTime) {
    try {
      sortRegister();

      if (startTime.isAfter(endTime)) {
        throw new IllegalArgumentException("Start time can not be after end time");
      }
      ArrayList<String[]> departureTableData = new ArrayList<>();

      List<TrainDeparture> departuresWithinTimeFrame = getRegisterArray().stream()
              .filter(trainDeparture -> trainDeparture.getDepartureTime().isAfter(startTime)
                      && trainDeparture.getDepartureTime().isBefore(endTime))
              .collect(Collectors.toList());

      for (TrainDeparture departure : departuresWithinTimeFrame) {
        departureTableData.add(departure.toTable());
      }

      return departureTableData;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return getDepartureTableData();
    }
  }

  /**
   * Verifies that a TrainDeparture with the given train number is
   * present in the departure register.
   *
   * @param trainNumber the train number to search for
   * @return true if a TrainDeparture with trainNumber is found, false otherwise
   */
  public boolean checkDepartureExistence(int trainNumber) {
    try {
      if (Validator.validatePositiveInt(trainNumber) == false) {
        throw new IllegalArgumentException("Train number must be a positive number.");
      }
      return this.departureRegister.stream()
              .anyMatch(trainDeparture -> trainNumber == trainDeparture.getTrainNumber());
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  /**
   * Edits the departure time of the TrainDeparture with the given train number.
   * If a TrainDeparture with the given train number is not found, the methode returns false.
   *
   * @param trainNumber   the train number of the TrainDeparture to edit
   * @param track         the new track number
   * @param delay         potential delay
   * @return true if the departure was successfully edited, false otherwise
   */
  public boolean editDeparture(int trainNumber, int track, LocalTime delay) {
    try {
      if (Validator.validatePositiveInt(trainNumber) == false) {
        throw new IllegalArgumentException("Train number must be a positive number.");
      }
      if (checkDepartureExistence(trainNumber) == false) {
        return false;
      }
      if (Validator.validateTrackInt(track) == false) {
        throw new IllegalArgumentException("Track must be a positive number or -1");
      }

      TrainDeparture departure = getDepartureByTrainNumber(trainNumber);
      departure.setDelay(delay.getHour(), delay.getMinute());
      departure.setTrack(track);
      return true;
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
      return false;
    }
  }

  // ---- Private helper methods ----

  /**
   * <p>Sorts the departure register by departure time.</p>
   */
  private void sortRegister() {
    this.departureRegister.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
  }
}
